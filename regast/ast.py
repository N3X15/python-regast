from __future__ import annotations
from sre_constants import MAXREPEAT
from typing import Iterable, List, Optional, Set, Tuple, Union, cast

from regast.reparse import lazy_escape, lazy_escape_in_class

__all__ = [
    "Branch",
    "Capture",
    "Category",
    "Class",
    "Literal",
    "Node",
    "NodeWithMembersMixin",
    "Range",
    "Repeat",
    "SubPattern",
]


class Node:
    FIELDS: Tuple[str, ...]

    def __init__(self, parent: Optional["Node"] = None) -> None:
        self.parent: Optional["Node"] = parent

    def __str__(self) -> str:
        raise NotImplemented


class Literal(Node):
    FIELDS = ("value",)

    def __init__(
        self, parent: Optional[Node] = None, value: str = "", negated: bool = False
    ) -> None:
        super().__init__(parent)
        self.value: str = value
        self.negated: bool = negated

    def __str__(self) -> str:
        return ("^" if self.negated else "") + (
            lazy_escape_in_class(self.value)
            if isinstance(self.parent, Class)
            else lazy_escape(self.value)
        )


class Range(Node):
    FIELDS = ("start", "end")

    def __init__(
        self, parent: Optional[Node] = None, start: str = "", end: str = ""
    ) -> None:
        super().__init__(parent)
        self.start: str = start
        self.end: str = end

    def __str__(self) -> str:
        a = lazy_escape_in_class(self.start)
        b = lazy_escape_in_class(self.end)
        return f"{a}-{b}"


class Category(Node):
    FIELDS = ("id",)

    def __init__(self, parent: Optional[Node] = None, id: str = "") -> None:
        super().__init__(parent)
        self.id: str = id

    def __str__(self) -> str:
        return f"\\{self.id}"


class NodeWithMembersMixin:
    ALLOWED_CHILDREN: Tuple[type, ...]

    def _handle_members(self, members: Iterable[Node] = []) -> None:
        self.members: List[Node] = []
        for m in members:
            assert isinstance(
                m, self.ALLOWED_CHILDREN
            ), f"{m} not allowed in {self.__class__.__name__}"
            self.members.append(m)


class Class(NodeWithMembersMixin, Node):
    FIELDS = ("members",)
    ALLOWED_CHILDREN = (Literal, Category, Range)

    def __init__(
        self,
        parent: Optional[Node] = None,
        members: Iterable[Node] = [],
        negated: bool = False,
    ) -> None:
        super().__init__(parent)
        self.negated: bool = negated
        self._handle_members(members)

    def __str__(self) -> str:
        o = "".join(map(str, self.members))
        return f"[{o}]"


class Repeat(NodeWithMembersMixin, Node):
    FIELDS = ("min", "max", "members")
    ALLOWED_CHILDREN: Tuple[type, ...] = (Literal, Category, Range, Class)

    def __init__(
        self,
        parent: Optional[Node] = None,
        min: int = 0,
        max: int = MAXREPEAT,
        members: Iterable[Node] = [],
    ) -> None:
        super().__init__(parent)
        self.min: int = min
        self.max: int = max
        self._handle_members(members)

    def __str__(self) -> str:
        o = "".join(map(str, self.members))
        if self.min > 0 and self.min == self.max:
            o += f"{{{self.min}}}"
        elif self.min == 0 and self.max == 1:
            o += "?"
        elif self.max == MAXREPEAT:
            if self.min == 0:
                o += "*"
            elif self.max == 1:
                o += "+"
            else:
                o += f"{{{self.min},}}"
        else:
            o += f"{{{self.min},{self.max}}}"
        return o


class SubPattern(NodeWithMembersMixin, Node):
    FIELDS = ("members",)

    def __init__(
        self,
        parent: Optional[Node] = None,
        group_id: Optional[Union[int, str]] = None,
        add_flags: int = 0,
        rem_flags: int = 0,
        members: Iterable[Node] = [],
    ) -> None:
        super().__init__(parent)
        self._handle_members(members)
        self.add_flags: int = add_flags
        self.rem_flags: int = rem_flags
        self.group_id: Optional[Union[int, str]] = group_id

    def __str__(self) -> str:
        o = "".join(map(str, self.members))
        if self.group_id is not None:
            if self.group_id is str:
                o = f"(?P<{self.group_id}>{o})"
            elif self.group_id is int:
                o = f"({o})"
        return o


class Anchor(Node):
    def __init__(self, parent: Node | None = None, id: str = "") -> None:
        super().__init__(parent)
        self.id: str = id

    def __str__(self) -> str:
        return self.id


class Pattern(NodeWithMembersMixin, Node):
    def __init__(self, parent: Node | None = None) -> None:
        super().__init__(parent)

    def __str__(self) -> str:
        return "".join(map(str, self.members))


class Capture(NodeWithMembersMixin, Node):
    def __init__(
        self,
        parent: Node | None = None,
        members: Iterable[Node] = [],
    ) -> None:
        super().__init__(parent)
        self._handle_members(members)
        self.group_id: Optional[Union[str, int]] = None

    def __str__(self) -> str:
        o = "".join(map(str, self.members))
        p: str = ""
        if self.group_id is not None:
            if self.group_id is str:
                p = f"?P<{self.group_id}>"
            elif self.group_id is int:
                p = ""
        return f"({p}{o})"


class Branch(Node):

    def __init__(
        self,
        parent: Node | None = None,
        subpatterns: Iterable[Node] = [],
    ) -> None:
        super().__init__(parent)
        self.subpatterns: Iterable[Node] = []

    def __str__(self) -> str:
        o: str = ""
        if self.parent is not Capture:
            o += "(?:"
        o += "|".join(map(str, self.subpatterns))
        if self.parent is not Capture:
            o += ")"
        return o


Class.ALLOWED_CHILDREN = (Literal, Category, Range)
Repeat.ALLOWED_CHILDREN = (
    Literal,
    Category,
    Range,
    Class,
    Repeat,
    SubPattern,
    Anchor,
)
SubPattern.ALLOWED_CHILDREN = (
    Literal,
    Category,
    Range,
    Class,
    Repeat,
    SubPattern,
    Anchor,
)
