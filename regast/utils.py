import inspect
import re
from typing import Any


def print_var(v: Any) -> None:
    varname: str = "UNKNOWN"
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r"\bprint_var\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)", line)
        if m:
            varname = m.group(1)
            break
    print(f"{varname}={v!r}")
