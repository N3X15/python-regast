import re
from sre_constants import (
    ANY,
    ASSERT_NOT,
    AT,
    AT_BEGINNING,
    AT_BEGINNING_LINE,
    AT_BEGINNING_STRING,
    AT_BOUNDARY,
    AT_END,
    AT_END_LINE,
    AT_END_STRING,
    AT_LOC_BOUNDARY,
    AT_LOC_NON_BOUNDARY,
    AT_NON_BOUNDARY,
    AT_UNI_BOUNDARY,
    AT_UNI_NON_BOUNDARY,
    BRANCH,
    CATEGORY,
    CATEGORY_DIGIT,
    CATEGORY_NOT_DIGIT,
    CATEGORY_NOT_SPACE,
    CATEGORY_NOT_WORD,
    CATEGORY_SPACE,
    CATEGORY_WORD,
    IN,
    LITERAL,
    MAX_REPEAT,
    MAXREPEAT,
    NOT_LITERAL,
    RANGE,
    SUBPATTERN,
)
from sre_parse import CATEGORIES, SubPattern, parse
from typing import Any, Dict, List, Optional, Set, Tuple

from regast.utils import print_var

ALL_SPECIAL_CHARS: str = "$()*+-.?[\\]^{}"
ESCAPED: Set[str] = {
    r"\a",
    r"\b",
    r"\f",
    r"\n",
    r"\r",
    r"\t",
    r"\v",
    r"\\",
}
ESCAPES: Dict[str, str] = {
    "\t": r"\a",
    "\b": r"\b",
    "\f": r"\f",
    "\n": r"\n",
    "\r": r"\r",
    "\t": r"\t",
    "\v": r"\v",
    "\\": r"\\",
}

REVERSE_ATCODES: Dict[int, str] = {
    AT_BEGINNING_LINE: r"^",
    AT_BEGINNING_STRING: r"\A",
    AT_BEGINNING: r"^",
    AT_BOUNDARY: r"\b",
    AT_END_LINE: r"$",
    AT_END_STRING: r"\Z",
    AT_END: r"$",
    AT_LOC_BOUNDARY: r"\b",
    AT_LOC_NON_BOUNDARY: r"\B",
    AT_NON_BOUNDARY: r"\B",
    AT_UNI_BOUNDARY: r"\b",
    AT_UNI_NON_BOUNDARY: r"\B",
}

REVERSE_CATCODES: Dict[int, str] = {
    CATEGORY_DIGIT: r"\d",
    CATEGORY_NOT_DIGIT: r"\D",
    CATEGORY_NOT_SPACE: r"\S",
    CATEGORY_NOT_WORD: r"\W",
    CATEGORY_SPACE: r"\s",
    CATEGORY_WORD: r"\w",
}


def __lazy_escape_base(
    s: str, escapes: Dict[str, str], categories: List[str], special_chars: str
) -> str:
    len_s = len(s)
    if len_s == 0:
        return ""
    if len_s > 1:
        return "".join(map(lazy_escape, list(s)))
    # print(s,ESCAPED)
    if s in escapes:
        return ESCAPES[s]
    if s in categories:
        return s
    if s not in special_chars:
        return s
    return re.escape(s)


_special_chars_map = {i: "\\" + chr(i) for i in b"()[]{}?*+-|^$\\.&~# \t\n\r\v\f"}

__BASIC_LE_ESCAPES: Dict[str, str] = dict(**ESCAPES)
__BASIC_LE_CATEGORIES: Set[str] = set(CATEGORIES.keys())
__BASIC_LE_SPECIALCHARS: str = "()[]{}?*+|^$\\.&~#"


def lazy_escape(s: str) -> str:
    return __lazy_escape_base(
        s,
        escapes=__BASIC_LE_ESCAPES,
        categories=__BASIC_LE_CATEGORIES,
        special_chars=__BASIC_LE_SPECIALCHARS,
    )


__CLASS_LE_ESCAPES: Dict[str, str] = dict(**ESCAPES)
__CLASS_LE_CATEGORIES: Set[str] = set(CATEGORIES.keys())
__CLASS_LE_SPECIALCHARS: str = "()[]{}-|^$\\.&~#"


def lazy_escape_in_class(s: str) -> str:
    return __lazy_escape_base(
        s,
        escapes=__CLASS_LE_ESCAPES,
        categories=__CLASS_LE_CATEGORIES,
        special_chars=__CLASS_LE_SPECIALCHARS,
    )


def _lazy_escape_range(t: Tuple[str, str]) -> str:
    a, b = t
    e = lazy_escape_in_class
    return f"{e(a)}-{e(b)}"


def _parse_in(v: List[Tuple[int, Any]], level: int = 0, debug: bool = False) -> str:
    literals: List[str] = []
    categories: List[str] = []
    # atcodes: List[str] = []
    ranges: List[Tuple[str, str]] = []

    negated: bool = False
    for op, a in v:
        assert op is not IN
        if op is LITERAL:
            literals.append(chr(a))
        elif op is NOT_LITERAL:
            negated = True
            literals.append(chr(a))
        # elif op is AT:
        #     atcodes.append(REVERSE_ATCODES[a])
        elif op is CATEGORY:
            categories.append(REVERSE_CATCODES[a])
        elif op is RANGE:
            start, end = a
            ranges.append((chr(start), chr(end)))
        else:
            print("BUG: UNHANDLED _parse_in OP", op.name, repr(a))
    if len(categories) == 1 and len(literals) == 0 and len(sorted(ranges)) == 0:
        return categories[0]
    classContent = (
        sorted(categories)
        + list(map(lazy_escape_in_class, sorted(literals)))
        + list(map(_lazy_escape_range, sorted(ranges)))
    )
    return "[" + ("^" if negated else "") + ("".join(classContent)) + "]"


def lazy_escape_chr(charval: int) -> str:
    return lazy_escape(chr(charval))


def _unparse_pattern(
    p: SubPattern,
    pdata: Optional[List[Tuple[int, Any]]] = None,
    level: int = 0,
    group_if_needed: bool = False,
    debug: bool = False,
    root_level: Optional[int] = None,
) -> str:
    if root_level is None:
        root_level = level
    gid2name = {v: k for k, v in p.state.groupdict.items()}
    o: str = ""
    if pdata is None:
        pdata = p.data
    things_needing_grouped: int = 0
    for op, data in pdata:
        if op is SUBPATTERN:
            gid, add_flags, del_flags, p_ = data
            if debug:
                print(
                    level * "  " + "SUBPATTERN",
                    gid,
                    gid2name.get(gid),
                    add_flags,
                    del_flags,
                )
            o += "("
            if gid in gid2name:
                o += f"?P<{gid2name[gid]}>"
            elif gid is None:
                o += "?:"
            o += _unparse_pattern(
                p_, level=level + 1, root_level=root_level, debug=debug
            )
            o += ")"
            things_needing_grouped += 1
        elif op is ANY:
            if debug:
                print(
                    level * "  " + "ANY",
                )
            o += "."
            things_needing_grouped += 1
        elif op is AT:
            if debug:
                print(level * "  " + "AT", repr(data))
            o += REVERSE_ATCODES[data]
            things_needing_grouped += 1
        elif op is IN:
            if debug:
                print(level * "  " + "IN", repr(data))
            o += _parse_in(data, level=level + 1, debug=debug)
            things_needing_grouped += 1
        elif op is LITERAL:
            if debug:
                print(level * "  " + "LITERAL", repr(data))
            o += lazy_escape_chr(data)
            things_needing_grouped += 1
        elif op is NOT_LITERAL:
            if debug:
                print(level * "  " + "NOT_LITERAL", repr(data))
            o += "[^" + lazy_escape_chr(data) + "]"
            things_needing_grouped += 1
        elif op is MAX_REPEAT:
            if debug:
                print(level * "  " + "MAX_REPEAT", repr(data[0]), repr(data[1]))
            min, max, item = data
            suffix = f'{{{min},{"" if max is MAXREPEAT else max}}}'
            if min == max:
                suffix = f"{{{min}}}"
            if min == 0 and max == 1:
                suffix = "?"
            elif max is MAXREPEAT:
                if min == 0:
                    suffix = "*"
                elif min == 1:
                    suffix = "+"
            o += (
                _unparse_pattern(
                    p,
                    item,
                    level=level + 1,
                    root_level=root_level,
                    group_if_needed=True,
                    debug=debug,
                )
                + suffix
            )
            things_needing_grouped += 1
        elif op is BRANCH:
            if debug:
                print(level * "  " + "BRANCH", repr(data[0]), repr(data[1]))
            if level == root_level:
                o += "(?:"
            for i, v in enumerate(data[1]):
                if i > 0:
                    o += "|"
                o += _unparse_pattern(
                    p, v, level=level + 1, root_level=root_level, debug=debug
                )
            if level == root_level:
                o += ")"
            things_needing_grouped += 2
        elif op is ASSERT_NOT:
            if debug:
                print(level * "  " + "ASSERT_NOT", repr(data))
            lr, sps = data
            o += "(?<!" if lr == -1 else "(?>!"
            o += _unparse_pattern(
                p, sps, level=level + 1, root_level=root_level, debug=debug
            )
            o += ")"
            things_needing_grouped += 1
        else:
            print("BUG: UNHANDLED _unparse_pattern OP", op.name, repr(data))
    if things_needing_grouped > 1 and group_if_needed:
        o = f"(?:{o})"
    return o


def reparse_regex(pattern: str, debug: bool = False) -> str:
    if debug:
        print("reparse_regex", "->", pattern)
    s: SubPattern = parse(pattern)
    # s.dump()
    o = _unparse_pattern(s, debug=debug, level=1)
    if debug:
        print("reparse_regex", "<-", o)
    return o


def main() -> None:

    regex = r"abc[azfxc0-9](a|c|b|z)"
    print(regex)
    print(reparse_regex(regex))

    print_var(ALL_SPECIAL_CHARS)
    ALL_SPECIAL_CHARS_ = "".join(sorted(ALL_SPECIAL_CHARS))
    print_var(ALL_SPECIAL_CHARS_)


if __name__ == "__main__":
    main()
