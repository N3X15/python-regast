# regast

A **Reg**ular expression <abbr title="Abstract Syntax Tree">**AST**</abbr> for Python 3.10+.

Currently based on SRE Regular Expressions, as implemented in Python 3.10.

**Under Construction, APIs will change wildly**

## Installation

```shell
pip install -U git+https://gitlab.com/N3X15/python-regast.py
```
OR
```shell
poetry add [-G dev] git+https://gitlab.com/N3X15/python-regast.py
```

## Usage

### Reparsing a Regular Expression

This process:

1. Parses the regular expression into a `SubPattern` using `sre_parse.parse()`
1. Lets sre_compile optimize things a bit
1. Uses our own code to decompile the bytecode
1. Sorts character classes so they're somewhat stable between builds (pet peeve of mine)
1. Dumps the parsed bytecode back into a string

All of this is done with one method call:

```python
from regast.reparse import reparse_regex

pattern:str = r"^The.*S(?:p|a|i){3}n$"
optimized_pattern:str = reparse_regex(pattern)
print(optimized_pattern)
```
```
"^The.*S[aip]{3}n$"
```