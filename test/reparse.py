import unittest

from regast.reparse import lazy_escape, lazy_escape_in_class, reparse_regex


class TestReparse(unittest.TestCase):
    def test_lazy_escape(self) -> None:
        self.assertEqual(lazy_escape("\t"), r"\t")
        self.assertEqual(lazy_escape("{"), r"\{")
        self.assertEqual(lazy_escape("}"), r"\}")
        self.assertEqual(lazy_escape("-"), r"-")
        self.assertEqual(lazy_escape("/"), r"/")

    def test_lazy_escape_in_class(self) -> None:
        self.assertEqual(lazy_escape_in_class("\t"), r"\t")
        self.assertEqual(lazy_escape_in_class("{"), r"\{")
        self.assertEqual(lazy_escape_in_class("}"), r"\}")
        self.assertEqual(lazy_escape_in_class("-"), r"\-")
        self.assertEqual(lazy_escape_in_class("/"), r"/")

    def test_reparse(self) -> None:
        self.assertEqual(
            reparse_regex("(-?\\d+(?:\\.\\d+)?)f?"), "(-?\\d+(?:\\.\\d+)?)f?"
        )
        self.assertEqual(reparse_regex(r"abc+(?:[aB]a|[C]c)"), r"abc+(?:[Ba]a|Cc)")

        # README.md
        self.assertEqual(reparse_regex(r"^The.*S(?:p|a|i){3}n$"), "^The.*S[aip]{3}n$")
